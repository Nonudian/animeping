import { render } from "react-dom";
import App from "./components/App";


Object.assign(document.body.style, { margin: 0, backgroundColor: "#282c34" });

render(<App />, document.getElementById("root"));