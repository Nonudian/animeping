import { makeStyles, Fade, Paper, List } from "@material-ui/core"
import { AnimeList, MangaList, WebsiteList } from "../list/Lists"


const useStyles = makeStyles({
    container: {
        padding: 3,
        margin: "auto",
        width: "60%",
        backgroundColor: "#dcdee2"
    },
    mangaContainer: {
        width: "40%"
    }
})

export default function DataContainer({ categoryId, data, extractedData, onView, onUpdate, onDelete }) {
    const classes = useStyles()

    const mangaStyle = categoryId === 1 ? classes.mangaContainer : ``

    return (
        <Fade in timeout={500}>
            <List dense className={`${classes.container} ${mangaStyle}`} component={Paper} elevation={3}>
                {{
                    0: <AnimeList
                        data={data}
                        extractedList={extractedData.animes}
                        onView={onView}
                        onUpdate={onUpdate}
                        onDelete={onDelete}
                    />,
                    1: <MangaList
                        data={data}
                        extractedList={extractedData.mangas}
                        onView={onView}
                        onUpdate={onUpdate}
                        onDelete={onDelete}
                    />,
                    2: <WebsiteList data={data} /*onUpdate={onUpdate} onDelete={onDelete} */ />
                }[categoryId]}
            </List>
        </Fade>
    )
}