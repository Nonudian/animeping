import { AnimeForm } from "../forms/AnimeForm"
import { DeletionForm } from "../forms/DeletionForm"
import Dialog from "../utils/Dialog"


export default function DialogContainer({ isOpen, isDeletion, formId, initialData, onClose, onSubmit }) {

    const chooseContent = () => {
        if (isDeletion) return <DeletionForm id={formId} initialData={initialData} onSubmit={onSubmit} />
        return ({
            "animes": <AnimeForm id={formId} initialData={initialData} onSubmit={onSubmit} />,
            "mangas": <></>,
            "websites": <></>
        }[formId])
    }

    const category = formId.slice(0, -1)

    return (
        <Dialog
            isOpen={isOpen}
            formId={formId}
            title={`${category} ${isDeletion ? `deletion` : `form`}`}
            content={chooseContent()}
            onClose={onClose}
        />
    )
}