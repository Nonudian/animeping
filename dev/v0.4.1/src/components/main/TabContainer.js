import { makeStyles } from "@material-ui/core/styles"
import { Paper, Tabs, Tab } from "@material-ui/core"


const useStyles = makeStyles({
    paper: {
        ["@media (min-width:1650px)"]: { // eslint-disable-line no-useless-computed-key
            width: "30%"
        },
        backgroundColor: "#dcdee2",
        margin: "auto",
        width: "70%",
        marginBottom: 10
    },
    indicator: {
        backgroundColor: "#820a2c",
        paddingTop: 2
    },
    label: {
        color: "#282c34",
        textTransform: "capitalize",
        fontWeight: "bold",
        '&$selected': {
            color: "#820a2c",
        },
        fontSize: 20
    },
    selected: {}
})

export default function TabContainer({ categories, currentId, onChange }) {
    const classes = useStyles()

    const categoryTabs = () => {
        return categories.map((name, i) => (
            <Tab
                key={i}
                classes={{ root: classes.label, selected: classes.selected }}
                label={name}
            />
        ))
    }

    return (
        <Paper className={classes.paper} elevation={3}>
            <Tabs
                classes={{ indicator: classes.indicator }}
                value={currentId}
                onChange={onChange}
                textColor="primary"
                centered
                children={categoryTabs()}
            />
        </Paper>
    )
}