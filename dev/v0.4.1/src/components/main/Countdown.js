import { makeStyles } from "@material-ui/core/styles"
import { CircularProgress, Typography, Paper, Fade } from "@material-ui/core"
import { default as ReactCountdown } from "react-countdown"
import { useEffect, useState } from "react"
import axios from "axios"


const useStyles = makeStyles({
    paper: {
        ["@media (min-width:1650px)"]: { // eslint-disable-line no-useless-computed-key
            width: "30%"
        },
        backgroundColor: "#dcdee2",
        margin: "auto",
        width: "70%",
        height: "50px",
        marginBottom: 15,
        textAlign: "center"
    },
    progress: {
        margin: 10,
        color: "#820a2c",
        verticalAlign: "bottom"
    },
    countdown: {
        padding: 7,
        fontSize: 24
    },
    "@keyframes orangeBlinkEffect": {
        "0%": {
            color: "#282c34",
        },
        "100%": {
            color: "#ff7300",
        }
    },
    soonElapsed: {
        animation: "$orangeBlinkEffect linear 0.75s infinite alternate"
    },
    "@keyframes redBlinkEffect": {
        "0%": {
            color: "#282c34",
        },
        "100%": {
            color: "#ff0000",
        }
    },
    verySoonElapsed: {
        animation: "$redBlinkEffect linear 0.75s infinite alternate"
    }
})

const initialCountdown = 20 * 60 * 1000 //20min

export default function Countdown({ onExtract }) {
    const classes = useStyles()

    const [countdown, setCountdown] = useState(Date.now())
    const [completed, setCompleted] = useState(false)

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => resetCountdown(), [])

    const resetCountdown = () => {
        axios.get("http://localhost:5000/extraction")
            .then(({ data }) => {
                onExtract(data)
                setCompleted(false)
                setTimeout(() => setCountdown(Date.now() + initialCountdown), 500)
            })
    }

    const renderer = ({ formatted, ...props }) => {
        setTimeout(() => setCompleted(props.completed), 500)

        const { minutes, seconds } = formatted

        const soonElapsedStyle = (minutes < 2)
            ? (minutes < 1 && seconds <= 30)
                ? classes.verySoonElapsed
                : classes.soonElapsed
            : ``

        return (
            <>
                <Fade in={props.completed && completed} timeout={{ enter: 500, exit: 200 }} unmountOnExit>
                    <CircularProgress className={classes.progress} size={30} />
                </Fade>
                <Fade in={!props.completed && !completed} timeout={{ enter: 500, exit: 200 }} unmountOnExit>
                    <Typography className={`${classes.countdown} ${soonElapsedStyle}`}>
                        {minutes}:{seconds}
                    </Typography>
                </Fade>
            </>
        )
    }

    return (
        <Paper className={classes.paper} elevation={3}>
            <ReactCountdown
                key={countdown}
                date={countdown}
                renderer={renderer}
                onComplete={resetCountdown}
            />
        </Paper>
    )
}