import { FC, ReactElement, MouseEventHandler } from "react";
import { AnimeItem } from "./items/AnimeItem";
import { MangaItem } from "./items/MangaItem";
import { AnimeFormState } from "../forms/AnimeForm";


// server-side types for items
export interface TitledDatum {
    title: string;
    datum: Datum[];
    date: string;
}
export interface Datum {
    value: number;
    url: {
        requestUrl: string;
        displayUrl: string;
    };
}

export interface ItemProps<T> {
    onView(title: string, categoryId: number): MouseEventHandler<HTMLButtonElement>;
    onUpdate(data: T): MouseEventHandler<HTMLButtonElement>;
    onDelete({ title }: Pick<TitledDatum, "title">): MouseEventHandler<HTMLButtonElement>;
}

interface AnimeListProps extends ItemProps<AnimeFormState> {
    data: AnimeFormState[];
    extractedList: TitledDatum[];
}

interface MangaListProps extends ItemProps<any> {
    data: any[]; //TODO: handle manga items
    extractedList: TitledDatum[];
}
interface WebsiteListProps {
    data: any[];
}

export const AnimeList: FC<AnimeListProps> = ({ data, extractedList, onView, onUpdate, onDelete }): ReactElement => {
    const sortedData = data.sort((a, b) => new Date(a.broadcast).valueOf() - new Date(b.broadcast).valueOf()).reverse();

    return (
        <>
            {sortedData.map((item, i) => (
                <AnimeItem
                    key={i}
                    data={item}
                    extractedItem={extractedList.find(({ title }) => title === item.title)}
                    onView={onView}
                    onUpdate={onUpdate}
                    onDelete={onDelete}
                />
            ))}
        </>
    );
};

export const MangaList: FC<MangaListProps> = ({ data, extractedList, onView, onUpdate, onDelete }): ReactElement => {
    return (
        <>
            {data.map((item, i) => (
                <MangaItem
                    key={i}
                    data={item}
                    extractedItem={extractedList.find(({ title }) => title === item.title)}
                    onView={onView}
                    onUpdate={onUpdate}
                    onDelete={onDelete}
                />
            ))}
        </>
    );
};

export const WebsiteList: FC<WebsiteListProps> = ({ data/*, onUpdate, onDelete*/ }): ReactElement => {
    return (
        // data.map((row, i) => {
        //     const { filterOptions, requestOptions, ...others } = row
        //     return (
        //         <Row
        //             key={i}
        //             data={others}
        //         /*onUpdate={(e) => onUpdate(row)}*/
        //         /*onDelete={(e) => onDelete({ nameId: row.name })}*/
        //         >
        //             <Cell />{/* TODO : render filterOptions */}
        //             <Cell />{/* TODO : render requestOptions */}
        //         </Row>
        //     )
        // })
        <p style={{ fontFamily: "Trebuchet MS", textAlign: "center", fontWeight: "bold", fontSize: 18 }}>
            🔨 Page currently in development...
        </p>
    );
};