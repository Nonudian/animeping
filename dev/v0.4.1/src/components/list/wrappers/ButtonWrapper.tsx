import { MouseEventHandler, PropsWithChildren } from "react";
import { ButtonGroup, ListItemText } from "@material-ui/core";
import { withStyles, createStyles } from "@material-ui/styles";
import { Create, Delete } from "@material-ui/icons";
import IconButton from "../../utils/IconButton";
import { PropsWithStyles } from "../../utils/style-utils";


const styles = () => createStyles({
    button: {
        color: "#282c34",
    },
    buttonContainer: {
        minWidth: "4%",
        textAlign: "center"
    }
});

type ButtonWrapperProps = PropsWithStyles<PropsWithChildren<{
    onUpdate: () => MouseEventHandler<HTMLButtonElement>;
    onDelete: () => MouseEventHandler<HTMLButtonElement>;
}>, typeof styles>;

const ButtonWrapper = ({ onUpdate, onDelete, classes, children }: ButtonWrapperProps) => {
    const { button, buttonContainer } = classes;

    const buttons = (//TODO: use foreach to build each IconButton
        <ButtonGroup>
            <IconButton
                classes={{ button }}
                onClick={onUpdate}
                children={<Create fontSize="small" />}
            />
            <IconButton
                classes={{ button }}
                onClick={onDelete}
                children={<Delete fontSize="small" />}
            />
        </ButtonGroup>
    );

    return (
        <>
            {children}
            <ListItemText className={buttonContainer} primary={buttons} />
        </>
    );
};

export default withStyles(styles)(ButtonWrapper);