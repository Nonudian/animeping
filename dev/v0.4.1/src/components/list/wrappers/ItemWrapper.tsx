import { FC, ReactElement } from "react";
import { makeStyles, ListItem } from "@material-ui/core";


const useStyles = makeStyles({
    container: {
        gap: 20,
        //flexWrap: "wrap",
        justifyContent: "center"
    },
    "@keyframes blinkEffect": {
        "0%": {
            backgroundColor: "#dcdee2",
        },
        "100%": {
            backgroundColor: "#c7a3ad",
        }
    },
    extracted: {
        animation: "$blinkEffect linear 1s infinite alternate"
    }
});

interface ItemWrapperProps {
    isRecentlyExtracted: boolean;
}

export const ItemWrapper: FC<ItemWrapperProps> = ({ isRecentlyExtracted, children }): ReactElement => {
    const { container, extracted } = useStyles();

    return (
        <ListItem className={`${container} ${isRecentlyExtracted ? extracted : ``}`}>
            {children}
        </ListItem>
    );
};

