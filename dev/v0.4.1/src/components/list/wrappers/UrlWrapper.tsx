import { FC, ReactElement } from "react";
import { makeStyles, Link, ListItemText, LinkBaseProps } from "@material-ui/core";
import { UrlObjectProps } from "../../forms/inputs/UrlInput";
import { Datum } from "../Lists";


const useStyles = makeStyles({
    urlContainer: {
        minWidth: "10%",
        textAlign: "center"
    },
    link: {
        paddingLeft: 3,
        paddingRight: 3,
        color: "inherit",
        "&:hover": {
            textDecoration: "none",
            color: "#820a2c"
        }
    },
    "@keyframes blinkEffect": {
        "0%": {
            color: "#282c34",
        },
        "100%": {
            color: "#b50d3c",
        }
    },
    extracted: {
        animation: "$blinkEffect linear 1s infinite alternate"
    }
});

interface SymbolMapping { //TODO: move elsewhere, in a config file
    [hostname: string]: string;
}

const symbols: SymbolMapping = { //TODO: move elsewhere, in a config file
    "vostfree": "Vf",
    "extreme-down": "Ex",
    "mangahere": "Mh",
    "mangadex": "Md"
};

interface UrlWrapperProps extends LinkBaseProps {
    urls: UrlObjectProps[];
    extractedUrls: Datum[];
}

export const UrlWrapper: FC<UrlWrapperProps> = ({ urls, extractedUrls, onClick, children }): ReactElement => {
    const { link, extracted, urlContainer } = useStyles();

    const links = urls.map(({ displayUrl }, i) => {
        const linkStyle = `${link} ${(extractedUrls.map(({ url }) => url.displayUrl).includes(displayUrl) ? extracted : ``)}`;
        const hostname = new URL(displayUrl).hostname.split(".").slice(-2)[0];

        return (
            <Link
                key={i}
                className={linkStyle}
                href={displayUrl}
                target="_blank"
                rel="noreferrer"
                onClick={onClick}
                children={symbols[hostname]}
            />
        );
    });

    return (
        <>
            {children}
            <ListItemText className={urlContainer} primary={links} />
        </>
    );
};