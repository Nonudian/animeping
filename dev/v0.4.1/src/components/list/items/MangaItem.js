import { makeStyles, Typography, ListItemText } from "@material-ui/core"
import { isEmpty } from "lodash"
import { ItemWrapper } from "../wrappers/ItemWrapper"
import { UrlWrapper } from "../wrappers/UrlWrapper"
import ButtonWrapper from "../wrappers/ButtonWrapper"


const useStyles = makeStyles({
    infoContainer: {
        minWidth: "75%",
        paddingLeft: 5
    },
    textStyle: {
        fontWeight: "bold"
    },
    titleStyle: {
        fontSize: 18
    },
    lastChapterStyle: {
        color: "#820a2c"
    }
})

function truncate(str, n) {
    return (str.length > n) ? `${str.substr(0, n - 1)}...` : str
}

export function MangaItem({ data, extractedItem, onView, onUpdate, onDelete }) {
    const { infoContainer, textStyle, titleStyle, lastChapterStyle } = useStyles()
    const { title, lastChapter, urls } = data

    const InfoItem = () => (
        <ListItemText
            className={infoContainer}
            disableTypography
            primary={
                <>
                    <Typography component="span" className={`${textStyle} ${titleStyle}`}
                        children={truncate(title, 42)}
                    />
                    <Typography component="span"
                        children={` · Reading `}
                    />
                    <Typography component="span" className={`${textStyle} ${lastChapterStyle}`}
                        children={`${lastChapter}`}
                    />
                </>
            }
        />
    )

    // extracted data
    const extractedUrls = extractedItem?.datum ?? []

    return (
        <ItemWrapper isRecentlyExtracted={!isEmpty(extractedUrls)}>
            <ButtonWrapper onUpdate={() => onUpdate(data)} onDelete={() => onDelete({ title })}>
                <UrlWrapper urls={urls} extractedUrls={extractedUrls} onClick={() => onView(title, 1)}>
                    <InfoItem />
                </UrlWrapper>
            </ButtonWrapper>
        </ItemWrapper>
    )
}