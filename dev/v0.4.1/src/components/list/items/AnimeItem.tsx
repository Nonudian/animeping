import { makeStyles, withStyles, LinearProgress, Typography, ListItemText } from "@material-ui/core";
import { isEmpty } from "lodash";
import moment from "moment";
import { FC, ReactElement } from "react";
import { AnimeFormState } from "../../forms/AnimeForm";
import { ItemProps, TitledDatum } from "../Lists";
import ButtonWrapper from "../wrappers/ButtonWrapper";
import { ItemWrapper } from "../wrappers/ItemWrapper";
import { UrlWrapper } from "../wrappers/UrlWrapper";


const useStyles = makeStyles({
    infoContainer: {
        minWidth: "60%",
        paddingLeft: 5
    },
    dateContainer: {
        minWidth: "18%",
        textAlign: "center"
    },
    textStyle: {
        fontWeight: "bold"
    },
    titleStyle: {
        fontSize: 18
    },
    lastEpisodeStyle: {
        color: "#820a2c"
    },
    progressStyle: {
        margin: "4px 0 4px 0"
    }
});

const CustomizedProgress = withStyles({
    root: {
        borderRadius: 5,
    },
    colorPrimary: {
        backgroundColor: "#9fa7b5",
    },
    bar: {
        borderRadius: 5,
        backgroundColor: "#820a2c",
    }
})(LinearProgress);

const truncate = (str: string, n: number) => {
    return (str.length > n) ? `${str.substr(0, n - 1)}...` : str;
};

const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

interface AnimeItemProps extends ItemProps<AnimeFormState> {
    data: AnimeFormState;
    extractedItem?: TitledDatum;
}

export const AnimeItem: FC<AnimeItemProps> = ({ data, extractedItem, onView, onUpdate, onDelete }): ReactElement => {
    const { infoContainer, dateContainer, textStyle, titleStyle, lastEpisodeStyle, progressStyle } = useStyles();
    const { title, seasonNumber, lastEpisode, totalEpisodes, broadcast, urls } = data;

    // left container
    const InfoItem = () => (
        <ListItemText className={infoContainer} disableTypography
            primary={
                <>
                    <Typography component="span" className={`${textStyle} ${titleStyle}`}
                        children={truncate(title, 50)}
                    />
                    <Typography component="span"
                        children={` · Season ${seasonNumber} · Watching `}
                    />
                    <Typography component="span" className={`${textStyle} ${lastEpisodeStyle}`}
                        children={`${lastEpisode}`}
                    />
                    <Typography component="span"
                        children={`/${totalEpisodes > 0 ? totalEpisodes : `?`}`}
                    />
                </>
            }
            secondary={
                <div className={progressStyle}>
                    <CustomizedProgress
                        variant="determinate"
                        value={totalEpisodes > 0 ? (lastEpisode / totalEpisodes) * 100 : 0}
                    />
                </div>
            }
        />
    );

    // right container
    const DateItem = () => {
        const nextBroadcast = new Date(broadcast);
        const duration = moment.duration(moment(nextBroadcast).diff(moment()));

        const notFinishedAiring = lastEpisode !== totalEpisodes || totalEpisodes === 0;
        const timeLeft = duration.days() > 0
            ? `${duration.days()}d ${duration.hours()}h left`
            : (duration.hours() > 0
                ? `${duration.hours()}h ${duration.minutes()}m left`
                : (duration.minutes() > 0
                    ? `In less than ${duration.minutes()}m`
                    : `Available soon...`
                )
            );
        const airing = `Airing every ${days[nextBroadcast.getDay()]}`;

        return (
            <ListItemText
                className={dateContainer}
                primary={(notFinishedAiring && timeLeft) || `Finished airing`}
                secondary={notFinishedAiring && airing}
            />
        );
    };

    // extracted urls
    const extractedUrls = extractedItem?.datum ?? [];

    return (
        <ItemWrapper isRecentlyExtracted={!isEmpty(extractedUrls)}>
            <ButtonWrapper onUpdate={() => onUpdate(data)} onDelete={() => onDelete({ title })}>
                <UrlWrapper urls={urls} extractedUrls={extractedUrls} onClick={() => onView(title, 0)}>
                    <InfoItem />
                    <DateItem />
                </UrlWrapper>
            </ButtonWrapper>
        </ItemWrapper>
    );
};