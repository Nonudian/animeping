import { ReactElement, FC, ChangeEvent, useReducer } from "react";
import { FormGroup, FormGroupProps, makeStyles } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { Form, FormState, emptyUrlObject } from "./Form";
import { TitleInput, BroadcastInput } from "./inputs/TextInput";
import { SeasonInput, LastEpisodeInput, TotalEpisodeInput } from "./inputs/NumberInput";
import { UrlInput } from "./inputs/UrlInput";
import IconButton from "../utils/IconButton";


const useStyles = makeStyles({
    row: {
        display: "flex",
        flexFlow: "row"
    },
    separator: {
        fontSize: 20,
        color: "rgba(0, 0, 0, 0.54)",
        paddingLeft: 5,
        paddingRight: 5,
        fontFamily: "Trebuchet MS"
    },
    buttonContainer: {
        paddingTop: 3,
        paddingLeft: 3
    },
    button: {
        color: "#820a2c"
    }
});

export interface AnimeFormState extends FormState {
    seasonNumber: number;
    lastEpisode: number;
    totalEpisodes: number;
    broadcast: string;
}

const emptyFormState: AnimeFormState = {
    title: "",
    seasonNumber: 1,
    lastEpisode: 0,
    totalEpisodes: 0,
    broadcast: "",
    urls: [{ ...emptyUrlObject }]
};

const numberify = ({ title, seasonNumber, lastEpisode, totalEpisodes, broadcast, urls }: AnimeFormState) => {
    return {
        title: title,
        seasonNumber: Number(seasonNumber),
        lastEpisode: Number(lastEpisode),
        totalEpisodes: Number(totalEpisodes),
        broadcast: broadcast,
        urls: urls.map(({ formerEpisode, ...others }) => ({ ...others, formerEpisode: Number(formerEpisode) }))
    };
};

type ActionType =
    | { type: "updateValue"; name: string; payload: string | number; }
    | { type: "updateUrls"; name: string; payload: string | number; index: number; }
    | { type: "addUrl"; };

const reducer = (state: typeof emptyFormState, action: ActionType) => {
    switch (action.type) {
        case "updateValue": return { ...state, [action.name]: action.payload };
        case "updateUrls": {
            const urls = [...state.urls];
            urls[action.index][action.name] = action.payload;
            return { ...state, urls };
        }
        case "addUrl": {
            const urls = [...state.urls, { ...emptyUrlObject }];
            return { ...state, urls };
        }
        default: return state;
    }
};

interface AnimeFormProps extends FormGroupProps {
    initialData: AnimeFormState;
}

export const AnimeForm: FC<AnimeFormProps> = ({ id, initialData, onSubmit }): ReactElement => {
    const { row, separator, button, buttonContainer } = useStyles();

    const [formState, dispatch] = useReducer(reducer, initialData || emptyFormState);
    const { title, seasonNumber, lastEpisode, totalEpisodes, broadcast, urls } = formState;
    const isCreation = !initialData;

    const handleChangeValue = ({ target: { name, value } }: ChangeEvent<HTMLInputElement>) => {
        dispatch({ type: "updateValue", name, payload: value });
    };
    const handleChangeUrls = ({ target: { name, value } }: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) => {
        dispatch({ type: "updateUrls", name, payload: value, index });
    };
    const handleAddUrl = (_event: ChangeEvent<HTMLInputElement>) => dispatch({ type: "addUrl" });

    return (
        <Form
            id={id}
            itemId={title}
            data={numberify(formState)}
            method={isCreation ? "POST" : "PUT"}
            onSubmit={onSubmit}
        >
            <TitleInput disabled={!isCreation} value={title} onChange={handleChangeValue} />
            <SeasonInput value={seasonNumber} onChange={handleChangeValue} />
            <FormGroup className={row} row>
                <LastEpisodeInput value={lastEpisode} onChange={handleChangeValue} />
                <p className={separator}>/</p>
                <TotalEpisodeInput value={totalEpisodes} onChange={handleChangeValue} />
            </FormGroup>
            <BroadcastInput disabled={!isCreation} value={broadcast} onChange={handleChangeValue} />
            {urls.map((urlObject, i) => (
                <FormGroup className={row} key={i} row>
                    <UrlInput
                        separatorClass={separator}
                        disabled={!isCreation}
                        urlObject={urlObject}
                        onChange={(e) => handleChangeUrls(e, i)}
                    />
                    {urls.length - 1 === i &&
                        <IconButton
                            classes={{ container: buttonContainer, button: button }}
                            isDisabled={!isCreation}
                            onClick={handleAddUrl}
                            children={<Add />}
                        />
                    }
                </FormGroup>
            ))}
        </Form>
    );
};