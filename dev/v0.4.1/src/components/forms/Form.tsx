import { ReactElement, FC, FormEvent } from "react";
import { FormControl, FormGroupProps } from "@material-ui/core";
import axios, { AxiosResponse, Method } from "axios";
import { UrlObjectProps } from "./inputs/UrlInput";


const hostName = "http://localhost:5000/"; //TODO: move elsewhere, in a config file

export interface FormState {
    title: string;
    urls: UrlObjectProps[];
}

export const emptyUrlObject: UrlObjectProps = {
    requestUrl: "",
    displayUrl: "",
    formerEpisode: 0
};

interface FormProps extends FormGroupProps {
    itemId: string;
    data: FormState;
    method: Extract<Method, "POST" | "PUT" | "DELETE">;
}

export const Form: FC<FormProps> = ({ id, itemId, data, method, onSubmit, children }): ReactElement => {

    const url = `${hostName}${id}`;

    const fetchData = (request: Promise<AxiosResponse>) => {
        request.then(() => axios.get(url).then(({ data }) => onSubmit!(data)));
    };

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        switch (method) {
            case "POST": fetchData(axios.post(url, data)); return;
            case "PUT": fetchData(axios.put(`${url}/${itemId}`, data)); return;
            case "DELETE": fetchData(axios.delete(`${url}/${itemId}`)); return;
        }
    };

    return (
        <form id={id} onSubmit={handleSubmit}>
            <FormControl>
                {children}
            </FormControl>
        </form>
    );
};