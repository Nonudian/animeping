import { ReactElement, FC } from "react";
import { OutlinedTextFieldProps } from "@material-ui/core";
import { RequestUrl, DisplayURL } from "./TextInput";
import { FormerEpisodeInput } from "./NumberInput";


interface UrlObjectMapping {
    [name: string]: string | number;
}

export interface UrlObjectProps extends UrlObjectMapping {
    requestUrl: string;
    displayUrl: string;
    formerEpisode: number;
}

interface UrlInputProps extends Omit<OutlinedTextFieldProps, "variant"> {
    separatorClass: string;
    urlObject: UrlObjectProps;
}

export const UrlInput: FC<UrlInputProps> = ({ separatorClass, disabled, urlObject, onChange }): ReactElement => {
    const { requestUrl, displayUrl, formerEpisode } = urlObject;

    return (
        <>
            <RequestUrl disabled={disabled} value={requestUrl} onChange={onChange} />
            <p className={separatorClass}></p>
            <DisplayURL disabled={disabled} value={displayUrl} onChange={onChange} />
            <p className={separatorClass}></p>
            <FormerEpisodeInput value={formerEpisode} onChange={onChange} />
        </>
    );
};