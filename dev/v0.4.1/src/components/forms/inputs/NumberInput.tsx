import { ReactElement, FC } from "react";
import { TextField, OutlinedTextFieldProps, makeStyles } from "@material-ui/core";
import camelcase from "camelcase";


const useStyles = makeStyles({
    input: {
        flexBasis: "50%"
    }
});

interface NumberInputProps extends OutlinedTextFieldProps {
    readonly label: string;
    value: number;
    readonly min: number;
}

const NumberInput: FC<NumberInputProps> = ({ className, label, value, onChange, variant, min }): ReactElement => {
    return (
        <TextField
            className={className || ""}
            label={label}
            margin="dense"
            name={camelcase(label)}
            type="number"
            value={value || min}
            onChange={onChange}
            variant={variant}
            InputProps={{ inputProps: { min: min } }}
        />
    );
};

type SpecificNumberInputProps = Pick<NumberInputProps, "value" | "onChange">;


export const FormerEpisodeInput: FC<SpecificNumberInputProps> = ({ value, onChange }): ReactElement => {
    return (
        <NumberInput
            label="Former episode"
            value={value}
            onChange={onChange}
            variant="outlined"
            min={0}
        />
    );
};

export const TotalEpisodeInput: FC<SpecificNumberInputProps> = ({ value, onChange }): ReactElement => {
    const { input } = useStyles();

    return (
        <NumberInput
            className={input}
            label="Total episodes"
            value={value}
            onChange={onChange}
            variant="outlined"
            min={0}
        />
    );
};

export const LastEpisodeInput: FC<SpecificNumberInputProps> = ({ value, onChange }): ReactElement => {
    const { input } = useStyles();

    return (
        <NumberInput
            className={input}
            label="Last episode"
            value={value}
            onChange={onChange}
            variant="outlined"
            min={0}
        />
    );
};

export const SeasonInput: FC<SpecificNumberInputProps> = ({ value, onChange }): ReactElement => {
    return (
        <NumberInput
            label="Season number"
            value={value}
            onChange={onChange}
            variant="outlined"
            min={1}
        />
    );
};