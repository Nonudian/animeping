import { ReactElement, FC } from "react";
import { TextField, OutlinedTextFieldProps, makeStyles } from "@material-ui/core";
import camelcase from "camelcase";


const useStyles = makeStyles({
    root: {
        "& .MuiFormLabel-root.Mui-error": {
            color: "red !important"
        },
        "& .MuiOutlinedInput-root.Mui-error .MuiOutlinedInput-notchedOutline": {
            borderColor: "red !important",
            borderWidth: 1
        }
    }
});

interface TextInputProps extends OutlinedTextFieldProps {
    readonly label: string;
    value: string;
}

const TextInput: FC<TextInputProps> = ({ disabled, error, label, value, onChange, variant }): ReactElement => {
    const { root } = useStyles();

    return (
        <TextField
            className={root}
            disabled={disabled || false}
            error={error || false}
            label={label}
            margin="dense"
            multiline
            name={camelcase(label)}
            type="text"
            value={value}
            onChange={onChange}
            variant={variant}
        />
    );
};

type SpecificTextInputProps = Pick<TextInputProps, "disabled" | "value" | "onChange">;

export const DisplayURL: FC<SpecificTextInputProps> = ({ disabled, value, onChange }): ReactElement => {
    return (
        <TextInput
            disabled={disabled}
            // error={false}
            label="Display URL"
            value={value}
            onChange={onChange}
            variant="outlined"
        />
    );
};

export const RequestUrl: FC<SpecificTextInputProps> = ({ disabled, value, onChange }): ReactElement => {
    return (
        <TextInput
            disabled={disabled}
            // error={false}
            label="Request URL"
            value={value}
            onChange={onChange}
            variant="outlined"
        />
    );
};

export const BroadcastInput: FC<SpecificTextInputProps> = ({ disabled, value, onChange }): ReactElement => {
    return (
        <TextInput
            disabled={disabled}
            // error={false}
            label="Broadcast"
            value={value}
            onChange={onChange}
            variant="outlined"
        />
    );
};

export const TitleInput: FC<SpecificTextInputProps> = ({ disabled, value, onChange }): ReactElement => {
    return (
        <TextInput
            disabled={disabled}
            // error={false}
            label="Title"
            value={value}
            onChange={onChange}
            variant="outlined"
        />
    );
};