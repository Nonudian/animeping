import { FC, ReactElement } from "react";
import { FormGroupProps, Typography } from "@material-ui/core";
import { Form, FormState } from "./Form";


interface DeletionFormProps extends FormGroupProps {
    initialData: FormState;
}

export const DeletionForm: FC<DeletionFormProps> = ({ id, initialData, onSubmit }): ReactElement => {
    const { title } = initialData;

    return (
        <Form id={id} itemId={title} data={initialData} method={"DELETE"} onSubmit={onSubmit}>
            <Typography>Would you really want to remove <strong>{title}</strong> ?</Typography>
        </Form>
    );
};