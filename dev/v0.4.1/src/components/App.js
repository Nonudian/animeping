import { makeStyles, createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { useState, useEffect } from "react";
import get from "axios";
import Countdown from "./main/Countdown";
import TabContainer from "./main/TabContainer";
import DataContainer from "./main/DataContainer";
import DialogContainer from "./main/DialogContainer";
import IconButton from "./utils/IconButton";


const useStyles = makeStyles({
    addButtonContainer: {
        paddingTop: 6
    },
    addButton: {
        color: "#dcdee2",
        "&:hover": {
            backgroundColor: "rgba(130, 10, 44, 0.54)",
            transition: "background-color 0.5s",
        }
    }
});

const theme = createMuiTheme({
    typography: {
        // h1
        h1: {
            fontSize: 35,
            color: "#dcdee2",
            textAlign: "center",
            padding: 30
        },
        // all other components
        fontFamily: "Trebuchet MS",
        fontSize: 16
    }
});

const categories = ["animes", "mangas", "websites"];
const hostName = "http://localhost:5000/";

export default function App() {
    const classes = useStyles();

    const [state, setState] = useState({
        categoryId: 0,
        data: [],
        isLoading: false,
        isOpenDialog: false,
        formData: null,
        isDeletion: false,
        extractedData: { animes: [], mangas: [] }
    });

    const { data, categoryId, isLoading, isOpenDialog, formData, isDeletion, extractedData } = state;
    const updateState = (update) => setState({ ...state, ...update });
    const changeDocumentTitle = () => {
        const alertLength = extractedData.animes.length + extractedData.mangas.length;
        document.title = `${alertLength > 0 ? `(${alertLength}) ` : ``}Anime Ping`;
    };

    useEffect(() => {
        get(hostName + categories[categoryId]).then(response => updateState({ data: response.data, isLoading: false }));
        changeDocumentTitle();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [categoryId, extractedData]);

    const handleChange = (_event, value) => {
        if (categoryId !== value) updateState({ isLoading: true, categoryId: value });
    };
    const handleLoadFormData = (data) => updateState({ formData: data, isOpenDialog: true, isDeletion: false });

    //TODO: handle formData according to its interface, not just an alone id
    const handleDeleteData = (id) => updateState({ formData: id, isOpenDialog: true, isDeletion: true });
    const handleCloseDialog = () => updateState({ isOpenDialog: false });
    const handleUpdateData = (data) => updateState({ data: data });
    const handleOpenDialog = () => updateState({ formData: null, isOpenDialog: true, isDeletion: false });
    const handleExtractData = (data) => {
        if (data.animes.length > 0 || data.mangas.length > 0) {

            const limitDate = new Date();
            limitDate.setHours(limitDate.getHours() + 3);

            const oldData = Object.keys(extractedData).map(category => {
                return extractedData[category].filter(({ date }) => new Date(date) < limitDate);
            });

            updateState({
                extractedData: {
                    animes: [...oldData[0], ...data.animes],
                    mangas: [...oldData[1], ...data.mangas]
                }
            });
        }
    };
    const handleRemoveExtractedData = (title, categoryId) => {
        updateState({
            extractedData: {
                animes: (categoryId === 0) ? extractedData.animes.filter(item => item.title !== title) : extractedData.animes,
                mangas: (categoryId === 1) ? extractedData.mangas.filter(item => item.title !== title) : extractedData.mangas
            }
        });
    };

    return (
        <ThemeProvider theme={theme}>
            <Typography variant="h1">Anime Ping App</Typography>
            <Countdown onExtract={handleExtractData} />
            <TabContainer categories={categories} currentId={categoryId} onChange={handleChange} />
            {!isLoading && data.length > 0 && <DataContainer
                categoryId={categoryId}
                data={data}
                extractedData={extractedData}
                onView={handleRemoveExtractedData}
                onUpdate={handleLoadFormData}
                onDelete={handleDeleteData}
            />}

            {/* TODO: handle form dialog for websites and mangas ; condition to remove */}
            {categoryId === 0 && <DialogContainer
                isOpen={isOpenDialog}
                isDeletion={isDeletion}
                formId={categories[categoryId]}
                initialData={formData}
                onClose={handleCloseDialog}
                onSubmit={handleUpdateData}
            />}
            {categoryId === 0 && <IconButton
                classes={{ container: classes.addButtonContainer, button: classes.addButton }}
                onClick={handleOpenDialog}
                children={<Add />}
            />}
        </ThemeProvider>
    );
}