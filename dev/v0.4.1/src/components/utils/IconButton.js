import { makeStyles } from "@material-ui/core/styles"
import { IconButton as MuiIconButton } from "@material-ui/core"


const useStyles = makeStyles({
    div: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }
})

export default function IconButton({ classes = {}, formId = null, type = null, isDisabled = false, onClick, children }) {
    const classNames = useStyles()

    return (
        <div className={`${classNames.div} ${classes.container}`}>
            <MuiIconButton
                className={classes.button}
                size="small"
                type={type}
                form={formId}
                disabled={isDisabled}
                onClick={onClick}
            >
                {children}
            </MuiIconButton>
        </div>
    )
}