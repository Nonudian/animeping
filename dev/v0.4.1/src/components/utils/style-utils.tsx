import { StyleRules, WithStyles } from "@material-ui/core";


export type PropsWithStyles<T, S extends () => StyleRules> = T & WithStyles<ReturnType<S>>;