import { makeStyles } from "@material-ui/core/styles"
import { Dialog as MuiDialog, DialogTitle, DialogContent, DialogActions, ButtonGroup } from "@material-ui/core"
import { Close, Done } from "@material-ui/icons"
import IconButton from "./IconButton"


const useStyles = makeStyles({
    paper: {
        backgroundColor: "#dcdee2"
    },
    center: {
        color: "#820a2c",
        margin: "auto"
    },
    title: {
        textTransform: "uppercase",
        "& h2": {
            fontSize: 30
        }
    },
    button: {
        color: "#820a2c",
        "&:hover": {
            backgroundColor: "rgba(220, 222, 226, 0.54)",
            transition: "background-color 0.5s"
        }
    }
})

export default function Dialog({ isOpen, formId, title, content, onClose }) {
    const classes = useStyles()

    return (
        <MuiDialog open={isOpen} onClose={onClose} classes={{ paper: classes.paper }}>
            <DialogTitle className={`${classes.center} ${classes.title}`} children={title} />
            <DialogContent children={content} />
            <DialogActions>
                <ButtonGroup className={classes.center}>
                    <IconButton
                        classes={{ button: classes.button }}
                        onClick={onClose}
                        children={<Close fontSize="large" />}
                    />
                    <IconButton
                        classes={{ button: classes.button }}
                        formId={formId}
                        type="submit"
                        onClick={onClose}
                        children={<Done fontSize="large" />}
                    />
                </ButtonGroup>
            </DialogActions>
        </MuiDialog>
    )
}