import express, { Application, json, Request, Response, Router } from "express";
import cors from "cors";
import morgan from "morgan";
import AnimeController from "./routes/AnimeController";
import MangaController from "./routes/MangaController";
import WebsiteController from "./routes/WebsiteController";
import AnimeList from "./lists/AnimeList";
import MangaList from "./lists/MangaList";


export default class App {

    private readonly _port: number = 5000;
    private readonly _hostname: string = "127.0.0.1";
    private readonly _loggerType: string = "dev";
    private _app: Application = express();

    constructor() {
        this.initializeMiddlewares();
        this.initializeControllers();
    }

    private initializeMiddlewares(): void {
        this._app.use(cors());
        this._app.use(morgan(this._loggerType));
        this._app.use(json());
    }

    private initializeControllers() {
        // simplify this shit
        const [ac, mc, wc] = [new AnimeController(), new MangaController(), new WebsiteController()];

        [ac, mc, wc].forEach(({ _router }) => { this._app.use("/", _router); });

        // to move elsewhere later
        this._app.use("/",
            Router().get("/extraction", async (_req: Request, res: Response): Promise<void> => {
                res.json({
                    animes: await (ac._list as AnimeList).updateAllItems(),
                    mangas: await (mc._list as MangaList).updateAllItems()
                });
            })
        );
    }

    start(): void {
        this._app.listen(this._port, this._hostname, () => {
            console.log(`App listening on the port ${this._port}!`);
        });
    }

}

new App().start();