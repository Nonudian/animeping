import fetch from "node-fetch";


export async function fetchProvider(): Promise<string> {
    return fetch("https://ipinfo.io/json")
        .then(response => response.json())
        .then(({ org }) => org)
        .catch(() => "");
}