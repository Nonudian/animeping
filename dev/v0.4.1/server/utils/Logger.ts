
const foregroundColors = {
    reset: "\x1b[0m",
    red: "\x1b[31m%s",
    green: "\x1b[32m%s",
    cyan: "\x1b[36m%s",
    magenta: "\x1b[35m%s"
};

function display(emoji: string, color: string, message: string): void {
    console.log(color, emoji, message, foregroundColors.reset);
}

export function error(message: string): void {
    display(`❌ `, foregroundColors.red, message);
}
export function success(message: string): void {
    display(`✔️ `, foregroundColors.green, message);
}
export function info(message: string): void {
    display(`ℹ️ `, foregroundColors.cyan, message);
}
export function warn(message: string): void {
    display(`⚠️ `, foregroundColors.magenta, message);
}