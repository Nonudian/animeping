import { readdirSync, readFileSync, writeFileSync } from "fs";


const path: string = "./data/";
const filenames: string[] = readdirSync(path);

export function loadFile(index: number): any[] {
    return JSON.parse(readFileSync(path + filenames[index], "utf8"));
}

export function saveFile(index: number, object: any): void {
    writeFileSync(path + filenames[index], JSON.stringify(object, null, 4));
}