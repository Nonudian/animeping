import moment from "moment-timezone";


//check here if `broadcast =` is necessary or not
export function formattedISOBroadcast(rawBroadcast: string): string {
    const regex = /^(?<d>[^\s].+)s\sat\s(?<h>[0-1][0-9]|2[0-3]):(?<m>[0-5][0-9])\s\(JST\)$/;
    const { d, h, m } = rawBroadcast.match(regex)!.groups!;

    const timezone = "Asia/Tokyo";
    const today = moment().tz(timezone);

    let broadcast = moment(today)
        .day(parseInt(d) + 4)
        .hour(parseInt(h))
        .minutes(parseInt(m));
    if (broadcast.isBefore(today)) broadcast.add(1, "w"); //just here
    return broadcast.toISOString();
}

export function nextISOBroadcast(broadcast: string, broadcastDiff: number): string {
    return moment(broadcast).add(broadcastDiff, "w").toISOString();
}
