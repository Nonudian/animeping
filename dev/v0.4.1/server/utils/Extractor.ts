import AbortController, { AbortSignal } from "node-abort-controller";
import fetch, { Headers, RequestInit, Response } from "node-fetch";
import { Agent } from "https";
import { xml2json } from "xml-js";
import { JSDOM } from "jsdom";
import { error, success, warn } from "./Logger";
import { FilterOptions, RequestOptions } from "../lists/WebsiteList";


async function extractFromXML(response: Response, { regex, language }: FilterOptions, displayUrl: string): Promise<number> {
    let jsonData: any;
    try {
        jsonData = JSON.parse(xml2json(await response.text(), { compact: true, spaces: 4 }));
    } catch (e) {
        error(`Error when converting xml to json for <${displayUrl}>`);
        return -1;
    }

    if (language !== undefined) jsonData = jsonData.rss.channel.item.filter((item: any) => item.description._text.match(language));
    else jsonData = jsonData.rss.channel.item;

    if (jsonData === undefined) {
        error(`Error when filtering data per language for <${displayUrl}>`);
        return -1;
    }

    const greaterData = jsonData
        .map((item: any) => item.title._text.match(regex))
        .filter((matching: string[] | null) => matching !== null)
        .map((matching: string[]) => parseInt(matching[1]))
        .reduce((max: number, num: number) => num > max ? num : max, 0);

    if (greaterData === null) {
        error(`Error when matching data for <${displayUrl}>`);
        return -1;
    }

    return greaterData;
}

async function extractFromHTML(response: Response, { selector, regex }: FilterOptions, displayUrl: string): Promise<number> {
    const dom = new JSDOM(await response.text());
    const filteredData = dom.window.document.querySelector(selector!);

    if (filteredData === null || filteredData.textContent === null) {
        error(`Error when selecting data for <${displayUrl}>`);
        return -1;
    }

    const matchedData = filteredData.textContent.match(regex);

    if (matchedData === null) {
        error(`Error when matching data for <${displayUrl}>`);
        return -1;
    }

    return parseInt(matchedData[1]);
}

function buildRequestOptions({ method, headers, agent }: RequestOptions, signal: AbortSignal): RequestInit {
    return {
        headers: new Headers(headers),
        method: method,
        signal: signal,
        agent: new Agent(agent)
    };
}

async function fetchData(url: URL, options: RequestOptions, controller: AbortController): Promise<Response | any> {
    const timeoutId = setTimeout(() => controller.abort(), 30000); //abort after reached 30 seconds

    return await fetch(url, buildRequestOptions(options, controller.signal))
        .then(res => { clearTimeout(timeoutId); return res; })
        .catch(err => { error(err); controller.abort(); });
}

export async function extractData(url: string, displayUrl: string, format: string, filterOpt: FilterOptions, requestOpt: RequestOptions): Promise<number> {
    const controller = new AbortController();
    const response = await fetchData(new URL(url), requestOpt, controller);

    if (controller.signal.aborted || response.status >= 500) {
        error(`Request failed for <${url}>`);
        return -1;
    }
    success(`Request succeeded for <${url}>`);

    switch (format) {
        case "html": return await extractFromHTML(response, filterOpt, displayUrl);
        case "xml": return await extractFromXML(response, filterOpt, displayUrl);
        default:
            warn("Warning: specific format is not recognized. Extraction skipped");
            return -1;
    }
}