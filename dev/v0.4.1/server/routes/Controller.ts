import { Router, Request, Response } from "express";
import List from "../lists/List";


export default abstract class Controller<T> {

    protected readonly _path: string;
    readonly _list: List<T>;
    readonly _router: Router = Router();

    protected constructor(path: string, list: List<T>) {
        this._path = path;
        this._list = list;
        this.intializeRoutes();
    }

    private intializeRoutes() {
        this._router.get(this._path, this.get);
        this._router.post(this._path, this.post);
        this._router.put(`${this._path}/:id`, this.put);
        this._router.delete(`${this._path}/:id`, this.delete);
    }

    get = (_req: Request, res: Response): void => {
        res.json(this._list.items);
    };

    post = ({ body }: Request, res: Response): void => {
        this._list.add(body);
        res.end();
    };

    put = ({ params, body }: Request, res: Response): void => {
        this._list.set(params.id, body);
        res.end();
    };

    delete = ({ params }: Request, res: Response): void => {
        this._list.remove(params.id);
        res.end();
    };

}