import Controller from "./Controller";
import MangaList, { Manga } from "../lists/MangaList";


export default class MangaController extends Controller<Manga> {

    constructor() {
        super("/mangas", MangaList.instance());
    }

}