import Controller from "./Controller";
import WebsiteList, { Website } from "../lists/WebsiteList";


export default class WebsiteController extends Controller<Website> {

    constructor() {
        super("/websites", WebsiteList.instance());
    }

}