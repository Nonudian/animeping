import Controller from "./Controller";
import AnimeList, { Anime } from "../lists/AnimeList";


export default class AnimeController extends Controller<Anime> {

    constructor() {
        super("/animes", AnimeList.instance());
    }

}