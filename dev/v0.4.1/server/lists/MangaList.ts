import RequestableList, { Data, Url } from "./RequestableList";


export interface Manga {
    title: string;
    lastChapter: number;
    urls: Url[];
}

export default class MangaList extends RequestableList<Manga> {

    private static _instance: MangaList;

    private constructor() { super(1); }

    static instance(): MangaList {
        if (!MangaList._instance) MangaList._instance = new MangaList();
        return MangaList._instance;
    }

    protected requestingItems(): Manga[] {
        return this._items;
    }

    protected keepData({ title, lastChapter }: Manga, { value }: Data): boolean {
        const isDataKept = value > lastChapter;
        if (isDataKept) this.set(title, { lastChapter: value });
        return isDataKept;
    }

    add(item: Manga): void {
        item.urls.forEach((url: Url) => {
            if (url.displayUrl.length === 0) url.displayUrl = url.requestUrl;
        });
        super.add(item);
    }

    protected indexOf(identifier: string): number {
        return this._items.map(({ title }) => title).indexOf(identifier);
    }

}