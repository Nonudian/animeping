import List from "./List";
import { Url, Data } from "./RequestableList";
import { fetchProvider } from "../utils/ProviderManager";
import { extractData } from "../utils/Extractor";


export interface Agent { rejectUnauthorized: boolean; }
export interface Headers { [key: string]: string; }
export interface RequestOptions {
    method: string;
    headers: Headers;
    agent: Agent;
}
export interface FilterOptions {
    selector?: string; //html query
    regex: string;
    language?: string; //language regex
}
export interface Domain {
    name: string;
    blockedBy: string[];
}
export interface Website {
    name: string;
    type: number;
    domains: Domain[];
    format: string;
    filterOptions: FilterOptions;
    requestOptions: RequestOptions;
}

export default class WebsiteList extends List<Website> {

    private static _instance: WebsiteList;

    private constructor() { super(2); }

    static instance(): WebsiteList {
        if (!WebsiteList._instance) WebsiteList._instance = new WebsiteList();
        return WebsiteList._instance;
    }

    private websiteId(url: string): number {
        return this.indexOf(new URL(url).hostname.split(".").slice(-2)[0]);
    }

    private urlDomain(url: string): string {
        return new URL(url).hostname.split(".").slice(-1)[0];
    }

    private async keepUsableUrls(urls: Url[]): Promise<Url[]> {
        const provider = await fetchProvider();
        return urls.filter(({ requestUrl }) => {
            const { name, domains } = this.item(requestUrl);

            // TODO: block request for MangaDex (currenlty rebuilding their website)
            if (!name.localeCompare("mangadex")) return false;

            const domain = this.urlDomain(requestUrl);
            if (domains.find(({ name }) => !domain.localeCompare(name))!.blockedBy.includes(provider)) return false;

            return true;
        });
    }

    async requestUsableUrls(urls: Url[]): Promise<Data[]> {
        const usableUrls = await this.keepUsableUrls(urls);

        return (await Promise.all(usableUrls.map(async url => {
            const { requestUrl, displayUrl } = url;
            const { format, filterOptions, requestOptions } = this.item(requestUrl);

            return {
                value: await extractData(requestUrl, displayUrl, format, filterOptions, requestOptions),
                url: url
            };
        }))).filter(({ value }) => value > -1);
    }

    protected item(url: string): Website {
        return this._items[this.websiteId(url)];
    }

    protected indexOf(identifier: string): number {
        return this._items.map(({ name }) => name).indexOf(identifier);
    }

}