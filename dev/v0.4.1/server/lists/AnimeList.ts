import RequestableList, { Data, Url } from "./RequestableList";
import { nextISOBroadcast, formattedISOBroadcast } from "../utils/BroadcastManager";


export interface AnimeUrl extends Url { formerEpisode: number; }
export interface Anime {
    title: string;
    seasonNumber: number;
    lastEpisode: number;
    totalEpisodes: number;
    broadcast: string;
    urls: AnimeUrl[];
}

export default class AnimeList extends RequestableList<Anime> {

    private static _instance: AnimeList;

    private constructor() { super(0); }

    static instance(): AnimeList {
        if (!AnimeList._instance) AnimeList._instance = new AnimeList();
        return AnimeList._instance;
    }

    protected requestingItems(): Anime[] {
        return this._items.filter(item => {
            const { lastEpisode, totalEpisodes, broadcast } = item;
            return new Date(broadcast) < new Date() && (totalEpisodes === 0 || totalEpisodes > lastEpisode);
        });
    }

    protected keepData({ title, lastEpisode, broadcast }: Anime, { value, url }: Data): boolean {
        const realEpisode = value - (<AnimeUrl>url).formerEpisode;
        const weekNumber = realEpisode - lastEpisode;

        const isDataKept = weekNumber > 0;
        if (isDataKept) {
            this.set(title, {
                lastEpisode: realEpisode,
                broadcast: nextISOBroadcast(broadcast, weekNumber)
            });
        }
        return isDataKept;
    }

    add(item: Anime): void {
        item.broadcast = formattedISOBroadcast(item.broadcast);
        item.urls.forEach((url: AnimeUrl) => {
            if (url.displayUrl.length === 0) url.displayUrl = url.requestUrl;
        });
        super.add(item);
    }

    protected indexOf(identifier: string): number {
        return this._items.map(({ title }) => title).indexOf(identifier);
    }
}