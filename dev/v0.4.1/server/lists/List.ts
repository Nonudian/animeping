import { loadFile, saveFile } from "../utils/FileManager";


type AtLeastOne<T> = { [K in keyof T]: Pick<T, K> }[keyof T];

export default abstract class List<T> {

    private readonly _fileIndex: number;
    protected _items: T[];

    constructor(fileIndex: number) {
        this._fileIndex = fileIndex;
        this._items = loadFile(fileIndex);
    }

    get items(): T[] {
        return this._items;
    }

    private save(): void {
        saveFile(this._fileIndex, this._items);
    }

    protected item(identifier: string): T {
        return this._items[this.indexOf(identifier)];
    }

    add(item: T): void {
        this._items[this._items.length] = item;
        this.save();
    }

    set(identifier: string, properties: AtLeastOne<T>): void {
        const item = this.item(identifier);
        Object.keys(properties).filter(key => key in item).forEach(key => item[key] = properties[key]);
        this.save();
    }

    remove(identifier: string): void {
        const index = this.indexOf(identifier);
        if (index === -1) return;

        this._items.splice(index, 1);
        this.save();
    }

    protected abstract indexOf(identifier: string): number;

}