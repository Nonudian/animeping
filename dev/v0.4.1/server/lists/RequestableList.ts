import List from "./List";
import { Anime } from "./AnimeList";
import { Manga } from "./MangaList";
import WebsiteList from "./WebsiteList";


// TODO: remove title from TitledDatum after making single-item extraction on React client
interface TitledDatum { title: string, datum: Data[]; date: string; };
export interface Data { value: number, url: Url; }
export interface Url { requestUrl: string; displayUrl: string; }

export default abstract class RequestableList<T extends Anime | Manga> extends List<T> {

    private _websiteList: WebsiteList = WebsiteList.instance();

    protected async updateItem(item: T): Promise<TitledDatum> {
        const { title, urls } = item;

        const datum = await this._websiteList.requestUsableUrls(urls);

        return {
            title: title,
            datum: datum.filter(data => this.keepData(item, data)),
            date: new Date().toISOString()
        };
    }

    async updateAllItems(): Promise<TitledDatum[]> {
        return await Promise.all(this.requestingItems().map(async item => await this.updateItem(item)));
    }

    protected abstract keepData(item: T, data: Data): boolean;

    protected abstract requestingItems(): T[];

}